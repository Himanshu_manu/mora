using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public class GoogleAdmobController : MonoBehaviour {
#if UNITY_ANDROID
	string adUnitId = "ca-app-pub-5890885267440430/6174454550";
#elif UNITY_IPHONE
    string adUnitId = "ca-app-pub-5890885267440430/4382225303";
#else
	string adUnitId = "unexpected_platform";
#endif
    // defining the interstitialAd.
    InterstitialAd interstitial;


	public void RequestInterstitial()
	{

		// Initialize an InterstitialAd.
		interstitial = new InterstitialAd(adUnitId);
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder().Build();
		// Load the interstitial with the request.
		interstitial.LoadAd(request);
	}

	public void showInterstitial()
	{
		if (interstitial.IsLoaded()) {
			interstitial.Show();
		}

	}



}
