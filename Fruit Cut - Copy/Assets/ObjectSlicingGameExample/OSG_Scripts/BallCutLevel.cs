﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
/// <summary>
/// This Class handles the balls destruction, instantiation of ball gibs, and anything else related to the destruction of the balls.  The Main Method 
/// "CutBall(int)", is called by the OSGTouchSlicer when a collision/ray-cast occurs against a ball.
/// </summary>
public class BallCutLevel : MonoBehaviour
{
    [SerializeField]
    public int id;
    [SerializeField]
    public bool isLocked = false;



    public void LevelSelect()
    {
        if (!isLocked)
        {
            transform.GetChild(1).GetComponent<Text>().color = Color.green;
            switch (GameController.GameControllerInstance.gameModes)
            {
                case GameModes.RegularGameMode:
                    //if we are in regular gameMode, then if "Cutball" is called increment the RegularModeScore.
                    transform.parent.GetComponent<RegularModeLevel>().Levelgenerate(id);

                    break;

                case GameModes.ChillGameMode:
                    //if we are in relax gameMode, then if "Cutball" is called increment the ChillModeScore.
                    transform.parent.GetComponent<ChillModeLevelsSelector>().Levelgenerate(id);

                    break;
                default:

                    break;
            }
        }
    }
}
