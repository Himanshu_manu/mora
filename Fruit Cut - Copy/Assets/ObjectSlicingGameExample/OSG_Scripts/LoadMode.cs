﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadMode : MonoBehaviour {
    public void LoadLevel(int sceneToLoadIfNot)
    {
        ScreenFaderSingleton.Instance.FadeAndLoadSpecificLevel(sceneToLoadIfNot);
        SettingsAndPauseMenu.instance.PlaySoundOnclick();
    }
}
