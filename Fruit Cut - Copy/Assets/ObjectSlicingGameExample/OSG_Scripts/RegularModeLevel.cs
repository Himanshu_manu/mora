﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class RegularModeLevel : MonoBehaviour
{

    [SerializeField]
    GameController gameController;
    [SerializeField]
    LauncherController launcherController;
    [SerializeField]
    GameObject UiCanvas;
    [SerializeField]
    GameObject TrialRend;
    void Start()
    {
        UnlockLevel();
    }
    public void Levelgenerate(int levelNum)
    {
        PlayerPrefs.SetInt("RegularModeLevelPlayed", levelNum);
        switch (levelNum)
        {
            case 1:
                launcherController.timeBetweenLaunches = 4;
                launcherController.timeBetweenRandomLaunches = 10;
                launcherController.timeBetweenBombLaunches = 35;
                break;
            case 2:
                launcherController.timeBetweenLaunches = 3.5f;
                launcherController.timeBetweenRandomLaunches = 9;
                launcherController.timeBetweenBombLaunches = 30;
                break;
            case 3:
                launcherController.timeBetweenLaunches = 3f;
                launcherController.timeBetweenRandomLaunches = 8;
                launcherController.timeBetweenBombLaunches = 25;
                break;
            case 4:
                launcherController.timeBetweenLaunches = 2.5f;
                launcherController.timeBetweenRandomLaunches = 7;
                launcherController.timeBetweenBombLaunches = 20;
                break;
            case 5:
                launcherController.timeBetweenLaunches = 2;
                launcherController.timeBetweenRandomLaunches = 6;
                launcherController.timeBetweenBombLaunches = 15;
                break;

        }
        gameController.gameIsRunning = true;
        gameController.level = levelNum;
        UiCanvas.gameObject.SetActive(true);
        TrialRend.gameObject.SetActive(true);
        gameController.StartChoosedGame();
        SelectDojoBackground.instance.ChangeDojoBG(levelNum);
        this.gameObject.SetActive(false);

    }
    public void UnlockLevel()
    {
        int levels = PlayerPrefs.GetInt("RegularModeLevelUnlocked");


        int LevelPlayed = PlayerPrefs.GetInt("RegularModeLevelPlayed");

        int travese = 1;
        Color _color;
        if (ColorUtility.TryParseHtmlString("#0074FF", out _color))

        foreach (Transform item in transform)
            {
                if (travese > levels)
                    break;
                if (item.GetComponent<BallCutLevel>())
                {
                    item.GetChild(1).GetComponent<Text>().color = _color;
                    if (travese == LevelPlayed)
                        item.GetChild(1).GetComponent<Text>().color = Color.green;

                    item.GetComponent<BallCutLevel>().isLocked = false;
                    item.GetChild(0).GetChild(0).gameObject.SetActive(false);
                    travese++;
                }
            }
        ScreenFaderSingleton.Instance.UnlockBgs(levels);
    }
}