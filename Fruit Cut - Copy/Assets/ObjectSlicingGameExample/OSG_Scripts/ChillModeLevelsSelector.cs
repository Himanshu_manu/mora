﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChillModeLevelsSelector : MonoBehaviour
{
    [SerializeField]
    GameController gameController;
    [SerializeField]
    GameObject UiCanvas;
    [SerializeField]
    GameObject TrainRend;
    [SerializeField]
    GenericUIElementFade elementFadeTime;
    void Start()
    {
        //UiCanvas.gameObject.SetActive(false);
        UnlockLevel();
    }
    public void Levelgenerate(int levelNum)
    {
        PlayerPrefs.SetInt("ChillModeLevelPlayed", levelNum);
        switch (levelNum)
        {
            case 1:
                gameController.GameTimer = 180f;
                elementFadeTime._text = 180.ToString();
                break;
            case 2:
                gameController.GameTimer = 150f;
                elementFadeTime._text = 150.ToString();
                break;
            case 3:
                gameController.GameTimer = 120f;
                elementFadeTime._text = 120.ToString();
                break;
            case 4:
                gameController.GameTimer = 90f;
                elementFadeTime._text = 90.ToString();
                break;
            case 5:
                gameController.GameTimer = 60f;
                elementFadeTime._text = 60.ToString();
                break;

        }
        gameController.gameIsRunning = true;
        gameController.level = levelNum;
        UiCanvas.gameObject.SetActive(true);
        TrainRend.SetActive(true);
        gameController.StartChoosedGame();
        SelectDojoBackground.instance.ChangeDojoBG(levelNum);
        this.gameObject.SetActive(false);

    }

    public void UnlockLevel()
    {
        
        int Chilllevels = PlayerPrefs.GetInt("LevelUnlocked");
        int LevelPlayed = PlayerPrefs.GetInt("ChillModeLevelPlayed");
        int travese = 1;
        Color _color;
        if (ColorUtility.TryParseHtmlString("#0074FF", out _color))

        foreach (Transform item in transform)
            {
                if (travese > Chilllevels)
                    break;
                if (item.GetComponent<BallCutLevel>())
                {
                    item.GetChild(1).GetComponent<Text>().color = _color;
                    if (travese == LevelPlayed)
                        item.GetChild(1).GetComponent<Text>().color = Color.green;

                    item.GetComponent<BallCutLevel>().isLocked = false;
                    item.GetChild(0).GetChild(0).gameObject.SetActive(false);

                    travese++;
                }
            }
        ScreenFaderSingleton.Instance.UnlockBgs(Chilllevels);
    }

    IEnumerator FadePanel()
    {
        float elapsedTime = 0f;
        float duration = 2f;
        Image img = this.GetComponent<Image>();
        Color c = img.color;
        while (elapsedTime < duration)
        {
            elapsedTime += Time.deltaTime;
            c.a = 1.0f - Mathf.Clamp01(elapsedTime / duration);
            img.color = c;
            yield return null;
        }
        this.gameObject.SetActive(false);
    }
}


